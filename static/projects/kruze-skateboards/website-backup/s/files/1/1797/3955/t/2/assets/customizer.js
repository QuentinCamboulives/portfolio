var snapperFront = "M333.5,863.6c-3.9,15.5-8.2,23.2-10.6,26.7c-4.9,7-29.9,26-57.5,28.6 c-27.6-2.6-52.6-21.6-57.5-28.6c-2.4-3.5-6.7-11.2-10.6-26.7c-20.5-80.4-55.7-306-47.4-507.7c9.9-240.5,115.5-238.2,115.5-238.2 s105.6-2.4,115.5,238.2C389.2,557.6,353.9,783.3,333.5,863.6z";
var snapperBack = "M822.9,863.6c-3.9,15.5-8.2,23.2-10.6,26.7c-4.9,7-29.9,26-57.5,28.6 c-27.6-2.6-52.6-21.6-57.5-28.6c-2.4-3.5-6.7-11.2-10.6-26.7c-20.5-80.4-55.7-306-47.4-507.7c9.9-240.5,115.5-238.2,115.5-238.2 s105.6-2.4,115.5,238.2C878.6,557.6,843.4,783.2,822.9,863.6z";

var pikeBack = "M850.1,601C839.3,802.8,831,857,822.6,888.8c-8.3,31.8-5.8,24.9-23.3,31.8 c-17.5,7-47.4,19.6-47.4,19.6s-30-12.6-47.4-19.6c-17.5-7-15,0-23.3-31.8c-8.3-31.8-16.6-85.9-27.5-287.7s0-351.9,0-351.9 c25-205.8,98.2-199.8,98.2-199.8s73.2-6,98.2,199.8C850.1,249.1,860.9,399.2,850.1,601z";
var pikeFront = "M361.6,604.5c-10.8,201.8-19.1,255.9-27.5,287.7c-8.3,31.8-5.8,24.9-23.3,31.8 c-17.5,7-47.4,19.6-47.4,19.6s-30-12.6-47.4-19.6c-17.5-7-15,0-23.3-31.8c-8.3-31.8-16.6-85.9-27.5-287.7 c-10.8-201.8,0-351.9,0-351.9c25-205.8,98.2-199.8,98.2-199.8s73.2-6,98.2,199.8C361.6,252.6,372.4,402.7,361.6,604.5z";

var sharkFront = "M266,36.8c106.1,45.8,73.4,216.4,122.7,222.5c12.5,144.5,12.5,288.8,0,432.8 c-30.6,9.7-28.9,93.8-36.6,164.7c-6.7,62.2-4.3,105.5-86.2,105c-82.1,3.2-84.1-43.7-89.9-103.8c-6.8-70.5-2-156-36.7-165.9 c-12.2-146.1-12.1-290.4,0-432.8C190.6,253.1,148.4,82.4,266,36.8z";
var sharkBack = "M754.3,36.8C860.4,82.6,827.7,253.1,877,259.2c12.5,144.5,12.5,288.8,0,432.8 c-30.6,9.7-28.9,93.8-36.6,164.7c-6.7,62.2-4.3,105.5-86.2,105c-82.1,3.2-84.1-43.7-89.9-103.8c-6.8-70.5-2-156-36.7-165.9 c-12.2-146.1-12.1-290.4,0-432.8C678.8,253.1,636.6,82.4,754.3,36.8z";

var simpleLine1 = "465.5,486.6 465.5,973.1 69.5,973.1 69.5,683.2 ";
var simpleLine2 = "";
var simpleLine3 = "";

var forward1 = "262.6,503 447.6,610.3 447.6,983.8 83.6,983.8 83.6,610.3";
var forward2 = "";
var forward3 = "";

var tripleLineAngle1 = "87.3,617.5 436.3,463.5 436.3,500.5 87.3,655.5";
var tripleLineAngle2 = "85.3,663.5 434.3,509.5 434.3,546.5 85.3,701.5";
var tripleLineAngle3 = "88.3,707.5 437.3,553.5 437.3,590.5 88.3,745.5";

var tripleLineStraight1 = "105,546.5 426,546.5 426,579.5 104,579.5";
var tripleLineStraight2 = "106,586.5 427,586.5 427,619.5 105,619.5";
var tripleLineStraight3 = "106,626.5 427,626.5 427,659.5 105,659.5";

var doubleLine1 = "245.5,42 261.5,34.5 261.5,992.5 245.5,992.5";
var doubleLine2 = "269.5,34.5 285.5,40.7 285.5,992.5 269.5,992.5 ";
var doubleLine3 = "";

var triangle1 = "267,648 452,403 452,970 88,970 88,403";
var triangle2 = "";
var triangle3 = "";

// var shadeSnapper = "https://cdn.shopify.com/s/files/1/1797/3955/files/shade_snapper3.png?13544376405917765137"
// var shadeShark = "https://cdn.shopify.com/s/files/1/1797/3955/files/shade_shark_cf7654a7-09f3-4bf3-8c8e-83f49805d223.png?9419088980938692711";
// var shadePike = "https://cdn.shopify.com/s/files/1/1797/3955/files/shade_pike.png?2251985558181938736";

// var wheelsSnapper = "https://cdn.shopify.com/s/files/1/1797/3955/files/wheels_snapper.png?15369099688577941559";
// var wheelsShark = "https://cdn.shopify.com/s/files/1/1797/3955/files/wheels_Shark_2.png?14908870272165540542";
// var wheelsPike = "https://cdn.shopify.com/s/files/1/1797/3955/files/wheels_pike_4.png?9044975018770621441";

// var wheelsBlack1 = "https://cdn.shopify.com/s/files/1/1797/3955/files/wheels_pike_black_4.png?4070499464804247877";
// var wheelsBlack2 = "https://cdn.shopify.com/s/files/1/1797/3955/files/wheels_snapper_black_4.png?6737842753500527527";

var shadeSnapper = "../customizer_assets/shade_snapper3.png"
var shadeShark = "../customizer_assets/shade_shark_cf7654a7-09f3-4bf3-8c8e-83f49805d223.png";
var shadePike = "../customizer_assets/shade_pike.png";

var wheelsSnapper = "../customizer_assets/wheels_snapper.png";
var wheelsShark = "../customizer_assets/wheels_Shark_2.png";
var wheelsPike = "../customizer_assets/wheels_pike_4.png";

var wheelsBlack1 = "../customizer_assets/wheels_pike_black_4.png";
var wheelsBlack2 = "../customizer_assets/wheels_snapper_black_4.png";

$(".customizer-button-shape").click(function () {
    if ($(this).attr('id') == "shark") {
        $("#skate-shape-front").attr("d", sharkFront);
      	$("#shadeImage").attr("xlink:href", shadeShark);
      	$("#wheelsImage").attr("xlink:href", wheelsShark);
      if ($("#blacktrucks").hasClass("customizer-button-actif")) {$("#wheelsBlack").attr("xlink:href", wheelsBlack1);}
	      else {$("#wheelsBlack").attr("xlink:href", "");}
    } else if ($(this).attr('id') == "snapper") {
        $("#skate-shape-front").attr("d", snapperFront);
      	$("#shadeImage").attr("xlink:href", shadeSnapper);
        $("#wheelsImage").attr("xlink:href", wheelsSnapper);
       if ($("#blacktrucks").hasClass("customizer-button-actif")) {$("#wheelsBlack").attr("xlink:href", wheelsBlack2);}      
        else {$("#wheelsBlack").attr("xlink:href", "");}

    } else if ($(this).attr('id') == "pike") {
        $("#skate-shape-front").attr("d", pikeFront);
      	$("#shadeImage").attr("xlink:href", shadePike);
        $("#wheelsImage").attr("xlink:href", wheelsPike);
       if ($("#blacktrucks").hasClass("customizer-button-actif")) {$("#wheelsBlack").attr("xlink:href", wheelsBlack1);}      
          else {$("#wheelsBlack").attr("xlink:href", "");}

    }

    $(".customizer-button-shape").each(function() {
        $(this).removeClass("customizer-button-actif");
    });
    $(this).addClass("customizer-button-actif");
});


$(".customizer-button-graphic").click(function () {
  	
  	if ($(this).attr('id') == "nothing") {
        $("#graphic1").attr("points", "");
        $("#graphic2").attr("points", "");
        $("#graphic3").attr("points", "");
        $("#graphic-color-title").text("RIEN");
    } else {
        if ($(this).attr('id') == "tripleLineAngle") {
          $("#graphic1").attr("points", tripleLineAngle1);
          $("#graphic2").attr("points", tripleLineAngle2);
          $("#graphic3").attr("points", tripleLineAngle3);
      } else if ($(this).attr('id') == "simpleLine") {
          $("#graphic1").attr("points", simpleLine1);
          $("#graphic2").attr("points", simpleLine2);
          $("#graphic3").attr("points", simpleLine3);
      } else if ($(this).attr('id') == "tripleLineStraight") {
          $("#graphic1").attr("points", tripleLineStraight1);
          $("#graphic2").attr("points", tripleLineStraight2);
          $("#graphic3").attr("points",tripleLineStraight3);
      } else if ($(this).attr('id') == "doubleLine") {
          $("#graphic1").attr("points", doubleLine1);
          $("#graphic2").attr("points", doubleLine2);
          $("#graphic3").attr("points", doubleLine3);
      } else if ($(this).attr('id') == "triangle") {
          $("#graphic1").attr("points", triangle1);
          $("#graphic2").attr("points", triangle2);
          $("#graphic3").attr("points", triangle3);
      } else if ($(this).attr('id') == "forward") {
          $("#graphic1").attr("points", forward1);
          $("#graphic2").attr("points", forward2);
          $("#graphic3").attr("points", forward3);
      }
      var slides = document.getElementsByClassName("graphic-color");
		for(var i = 0; i < slides.length; i++)
			{
              if($(slides.item(i)).hasClass("customizer-button-actif")){
                 	var myId =  $(slides.item(i)).attr('id');
                    myId = "#" + myId.substring(1);
                $("#graphic-color-title").text($(myId).attr('value'));
			}
            }
           }
  
    $(".customizer-button-graphic").each(function() {
        $(this).removeClass("customizer-button-actif");
    });
    $(this).addClass("customizer-button-actif");
});

$(".graphic-color").click(function () {
    var color = $(this).css("backgroundColor");
 	var myId =  $(this).attr('id');
    	myId = "#" + myId.substring(1);

    if ($(this).attr('for') == "r1") {
      	$("#woodyBruh").attr("xlink:href", "../customizer_assets/Wood_texture_2.jpg");
  	    $("#PimpMyGraphic").addClass("invisibeul");
		$("#woodyBruh").removeClass("invisibeul");
    }
  	else if ($(this).attr('for') == "r2") {
      $("#woodyBruh").attr("xlink:href", "../customizer_assets/Wood_texture_5.jpg");
  	    $("#PimpMyGraphic").addClass("invisibeul");
		$("#woodyBruh").removeClass("invisibeul"); 	
  	}
  	else {
  	    $("#PimpMyGraphic").removeClass("invisibeul");
		$("#woodyBruh").addClass("invisibeul");
  	}
    $("#PimpMyGraphic").attr("fill", color);
    
  	$(".graphic-color").each(function() {
        $(this).removeClass("customizer-button-actif");
    });
    $(this).addClass("customizer-button-actif");
  	
  if($("#nothing").hasClass("customizer-button-actif")){
        $("#graphic-color-title").text("RIEN");
  }
  else {
    $("#graphic-color-title").text($(myId).attr('value'));
  }
});

$(".shape-color").click(function () {
    var color = $(this).css("backgroundColor");
 	var myId =  $(this).attr('id');
  
  	myId = "#" + myId.substring(1);
   	
  if ($(this).attr('for') == "d1") {
      	$("#mainBackgroundWood").attr("xlink:href", "../customizer_assets/Wood_texture_2.jpg");
    }
  	else if ($(this).attr('for') == "d2") {
      	$("#mainBackgroundWood").attr("xlink:href", "../customizer_assets/Wood_texture_5.jpg");
  	}
  
  	$(".displayed-shape-color").attr("fill", color);
    
  	$(".shape-color").each(function() {
        $(this).removeClass("customizer-button-actif");
    });
    $(this).addClass("customizer-button-actif");

  $("#shape-color-title").text($(myId).attr('value'));
});

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

$("#randomizer").click(function() {
  var r = getRandomInt(0, 2);
  if (r == 0) {
    $("#shark").trigger("click");
  } else if (r == 1) {
    $("#pike").trigger("click");
  } else if (r == 2) {
    $("#snapper").trigger("click");
  }
  
  r = getRandomInt(0, 14);

    if (r >= 0 && r <= 3){
    $("#ld1").trigger("click");
  } else if (r >= 4 && r <= 5) {
    $("#ld2").trigger("click");
  } else if (r == 6) {
    $("#ld3").trigger("click");
  } else if (r == 7) {
    $("#ld4").trigger("click");
  } else if (r == 8) {
    $("#ld5").trigger("click");
  } else if (r == 9) {
    $("#ld6").trigger("click");
  } else if (r == 10) {
    $("#ld7").trigger("click");
  } else if (r == 11) {
    $("#ld8").trigger("click");
  } else if (r == 12) {
    $("#ld9").trigger("click");
  } else if (r == 13) {
    $("#ld10").trigger("click");
  } else if (r == 14) {
    $("#ld11").trigger("click");
  }
  
  r = getRandomInt(0, 6);
  if (r == 1 || r == 0) {
    $("#simpleLine").trigger("click");
  } else if (r == 2) {
    $("#triangle").trigger("click");
  } else if (r == 3) {
    $("#tripleLineAngle").trigger("click");
  } else if (r == 4) {
    $("#tripleLineStraight").trigger("click");
  } else if (r == 5) {
    $("#doubleLine").trigger("click");
  } else if (r == 6) {
    $("#forward").trigger("click");
  }
   r = getRandomInt(0, 14);

  if (r == 0){
    $("#lr1").trigger("click");
  } else if (r == 1) {
    $("#lr2").trigger("click");
  } else if (r == 2) {
    $("#lr3").trigger("click");
  } else if (r == 3) {
    $("#lr4").trigger("click");
  } else if (r == 4) {
    $("#lr5").trigger("click");
  } else if (r == 5) {
    $("#lr6").trigger("click");
  } else if (r == 6) {
    $("#lr7").trigger("click");
  } else if (r == 7) {
    $("#lr8").trigger("click");
  } else if (r == 8) {
    $("#lr9").trigger("click");
  } else if (r == 9) {
    $("#lr10").trigger("click");
  } else if (r == 10) {
    $("#lr11").trigger("click");
  }
});

$(".trucks").click(function () {
  
  if ($(this).attr('id') == "blacktrucks") {
    if ($("#shark").hasClass("customizer-button-actif")){
        $("#wheelsBlack").attr("xlink:href", wheelsBlack1);
    } else if ($("#pike").hasClass("customizer-button-actif")){
        $("#wheelsBlack").attr("xlink:href", wheelsBlack1);
    } else { $("#wheelsBlack").attr("xlink:href", wheelsBlack2);}
   }
  else {
    $("#wheelsBlack").attr("xlink:href", "");
  }
  
  $(".trucks").each(function() {
        $(this).removeClass("customizer-button-actif");
    });
  $(this).addClass("customizer-button-actif");
});
