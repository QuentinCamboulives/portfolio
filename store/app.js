import { getField, updateField } from 'vuex-map-fields'

const getDefaultState = () => ({
  drawer: false
})

export const state = () => getDefaultState()

export const getters = {
  getField
}

export const actions = {
  snack({ commit }, payload) {
    commit('setSnack', payload)
  }
}

export const mutations = {
  updateField,

  resetState(currentState) {
    Object.assign(currentState, getDefaultState())
  }
}
