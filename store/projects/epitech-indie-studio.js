const EpitechIndieStudio = {
  url: 'epitech-indie-studio',
  createdIn: 2017,
  title: 'Xenon Racer',
  tagLine: 'Creation of a video game with the Irrlicht graphic library',
  description: [
    'The goal of this Epitech project was to develop a small video game in C++, with the obligation to use the Irrlicht graphics library.',
    'We had chosen to create a spaceship racing game called Xenon Racer, and implement a server for multiplayer, as well as a map editor allowing the player to build his own circuits from blocks that could be dragged and dropped.'
  ],
  imgs: [
    {
      src: '01.png',
      caption: 'Main menu'
    },
    {
      src: '02.png',
      caption: 'Options menu'
    },
    {
      src: '03.png',
      caption: 'Spacecraft selection'
    },
    {
      src: '04.png',
      caption: 'Circuit selection'
    },
    {
      src: '05.png',
      caption: 'A multiplayer race'
    },
    {
      src: '06.png',
      caption: 'The circuit editor'
    },
    {
      src: '07.png',
      caption: 'Preview of the circuit created with the editor'
    }
  ],
  tags: ['C++', 'Game design', 'Irrlicht', 'Epitech'],
  technos: ['C++']
}

export default EpitechIndieStudio
