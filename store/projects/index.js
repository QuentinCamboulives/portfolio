import { getField, updateField } from 'vuex-map-fields'
// import Vue from 'vue'

import HexaSpace from './hexaspace.js'
import OmegaWomen from './omega-women.js'
import RealTimeObjectRecognition from './real-time-object-recognition.js'
import TinyGarden from './tiny-garden.js'
import SmartPool from './smart-pool.js'
import KruzeSkateboards from './kruze-skateboards.js'
import EpitechArcade from './epitech-arcade.js'
import EpitechIndieStudio from './epitech-indie-studio.js'
import IssegProjectWeek from './project-week.js'
import EmtechHackathon from './emtech-hackathon.js'

const getDefaultState = () => ({
  project: null,
  projects: [
    HexaSpace,
    OmegaWomen,
    RealTimeObjectRecognition,
    TinyGarden,
    SmartPool,
    KruzeSkateboards,
    EpitechArcade,
    EpitechIndieStudio,
    IssegProjectWeek,
    EmtechHackathon
  ]
})

export const state = () => getDefaultState()

export const getters = {
  getField
}

export const actions = {
  getProject({ commit }, payload) {
    commit('getProject', payload)
  }
}

export const mutations = {
  updateField,

  resetState(currentState) {
    Object.assign(currentState, getDefaultState())
  },

  getProject(state, projectUrl) {
    const foundProject = state.projects.find(
      project => project.url === projectUrl
    )
    if (foundProject) {
      state.project = foundProject
    }
  }
}
