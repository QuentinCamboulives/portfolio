const SmartPool = {
  url: 'smart-pool',
  createdIn: 2017,
  title: 'Smart Pool',
  tagLine:
    'Automated and connected intelligent swimming pool model managed by an Intel Edison card',
  description: [
    'IoT project realized alone in about three weeks during my studies at Epitech in the scope of the Hub projects. The Hub was a kind of lab with computer hardware and it was a place where creating projects using electronic hardware was valued. It was nice since I already liked building these kinds of projects.',
    'I had the idea to do this project because I was thinking about automating everyday tasks. I had noticed that a swimming pool requires a lot of maintenance work that I thought could be fully automated. So I decided to build a fully connected and automated pool model.',
    'I started by making a wooden frame, then a plexiglass tank representing the pool, including a skimmer and a pump to simulate the filtration. I also placed a water level sensor and a tank with a second pump. When the equivalent of a glass of water was removed from the pool, the water level sensor detected it and ordered the second pump to level the pool from the water in the tank.',
    'I had also built a system of cover which opened and closed according to the time of the day and the weather. I used a rain sensor to trigger the closing of the cover if it detected drops in order to keep the pool water clean.But also a light sensor to detect the fall of the night and so to trigger the closing of the cover of protection.',
    'I created a chlorine dispenser with a stepper motor and a drill bit as a worm screw. It was filled with salt to simulate chlorine, and released a dose each day at the set time.',
    'The computer board I was using was an Intel Edison and it could not handle the current above 5V while my pumps and motors were in 12V, so I had to use relays and make a small motherboard to drive all these elements from the computer board.',
    'Finally I had developed a web interface served by the card, accessible from the Internet, allowing to monitor the pool remotely as well as to perform forced actions, and to take control over the automations.'
  ],
  imgs: [
    {
      src: '14.png',
      caption: 'Web-based swimming pool control interface'
    },
    {
      src: '1.jpg',
      caption: 'Construction of the base'
    },
    {
      src: '2.jpg',
      caption: 'Frame construction'
    },
    {
      src: '3.jpg',
      caption: 'Construction of the plexiglass pool'
    },
    {
      src: '7.jpg',
      caption: 'Motherboard engineering'
    },
    {
      src: '6.jpg',
      caption: 'Front side of the motherboard'
    },
    {
      src: '4.jpg',
      caption: 'Back side of the motherboard'
    },
    {
      src: '8.jpg',
      caption:
        'Installation of electronic components connected to the motherboard'
    },
    {
      src: '9.jpg',
      caption: 'A part of the inside of the model'
    },
    {
      src: '10.jpg',
      caption: 'Inside the finished model'
    },
    {
      src: '11.jpg',
      caption: 'Overview of the inside of the model'
    },
    {
      src: '12.jpg',
      caption: 'Plexiglass pool frame'
    },
    {
      src: '13.jpg',
      caption: 'Finished model, ready to use'
    }
  ],
  tags: [
    'IoT',
    'Raspberry Pi',
    'Electronics',
    'Sensors',
    'Prototyping',
    'UI',
    'UX'
  ],
  technos: ['Node.js', 'Express', 'Javascript', 'jQuery', 'HTML5', 'CSS3']
}

export default SmartPool
