const OmegaWomen = {
  url: 'omega-women',
  createdIn: 2022,
  title: 'OMEGA WOMEN - NFT Collection',
  tagLine:
    'An NFT collection of generative Art, made of 11111 living artworks generated from different traits of rarity',
  description: [
    'OMEGA WOMEN is a positive community that wants to make Web3 the starting point for an inclusive future where all women empower their lives.'
  ],
  imgs: [
    {
      src: 'front.png',
      caption:
        'Main menu of our project allowing to choose the graphic library and the game to load'
    },
    {
      src: 'omega-header.jpg',
      caption:
        'Main menu of our project allowing to choose the graphic library and the game to load'
    }
  ],
  tags: ['NFT', 'Web3', 'Smart Contract'],
  technos: ['Solidity', 'Nuxt.js', 'Vue.js', 'Javascript'],
  links: [
    {
      icon: 'icon-park:dot',
      title: 'OMEGA WOMEN Website',
      url: 'https://omega-women.art/'
    },
    {
      icon: 'simple-icons:opensea',
      title: 'OpenSea Collection',
      url: 'https://opensea.io/collection/omega-women'
    }
  ]
}
export default OmegaWomen
