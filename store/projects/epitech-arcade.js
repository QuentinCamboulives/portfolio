const EpitechArcade = {
  url: 'epitech-arcade',
  createdIn: 2017,
  title: 'Epitech Arcade',
  tagLine:
    'Creation of an arcade terminal using graphic libraries that can be swapped at runtime',
  description: [
    'The objective of this Epitech project was to re-code two known mini games that can run on two different graphic libraries (Ncurse & SFML). It was required to be able to switch libraries dynamically during a game. When switching graphics libraries, the game data is exported to the new graphics library.'
  ],
  imgs: [
    {
      src: 'arcade_mario.png',
      caption:
        'Main menu of our project allowing to choose the graphic library and the game to load'
    },
    {
      src: 'arcade_pacman.png',
      caption: 'Packman game loaded'
    },
    {
      src: 'arcade_snake.png',
      caption: 'Snake game loaded'
    }
  ],
  tags: ['C++', 'Game design', 'Ncurse', 'SFML', 'Epitech'],
  technos: ['C++']
}
export default EpitechArcade
