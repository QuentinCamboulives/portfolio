const KruzeSkateboards = {
  url: 'kruze-skateboards',
  createdIn: 2017,
  title: 'Kruze Skateboards',
  tagLine: 'Entrepreneurship project created with a friend during one summer',
  description: [
    'This project was born during a summer, with a friend we wanted to set up a small business to experience entrepreneurship as auto entrepreneurs. We wanted to sell a type of premium skateboard that could not be found in stores.',
    'We made the skateboards ourselves in a garage, from cutting the board to painting it, to assembly and shipping.',
    'For the sales site we used Shopify to build an efficient and secure online store and not have to develop the payment and order management. We had created three ranges from the three possible shapes of boards.',
    'I had also developed a custom plugin allowing the complete customization of a skateboard so that customers could order the board of their dreams. Since these skateboards took us more time, they were sold at a slightly higher price.',
    'The project remained active for about a year for a little more than 200 orders, then we closed the site for lack of free time during our studies.',
    'I advise you to go to the backup of our site (link in the info section) and play a little with the customizer to see by yourself the project, it is however possible that there are some bugs because the backup is a dynamic site that has been converted into static.'
  ],
  imgs: [
    {
      src: '04.png',
      caption: 'Homepage of our website'
    },
    {
      src: '05.png',
      caption: 'Overview of our skateboards types'
    },
    {
      src: '06.png',
      caption: 'Information for customers'
    },
    {
      src: '07.png',
      caption: 'Snapper cruisers collection'
    },
    {
      src: '08.png',
      caption: 'Shark cruisers collection'
    },
    {
      src: '09.png',
      caption: 'Pike cruisers collection'
    },
    {
      src: '10.png',
      caption: 'Overview of the skateboards we were selling'
    },
    {
      src: '11.png',
      caption: 'Details about our different models'
    },
    {
      src: '12.png',
      caption: 'Customizer page with an example of a custom skateboard'
    },
    {
      src: '13.png',
      caption: 'Customizer page with another example of a custom skateboard'
    },
    {
      src: '14.png',
      caption: 'Customizer page with another example of a custom skateboard'
    },
    {
      src: '15.png',
      caption: 'Customizer page with another example of a custom skateboard'
    },
    {
      src: '02.jpg',
      caption: 'A part of our studio with painted boards, ready to be assembled'
    },
    {
      src: '01.jpg',
      caption: 'A part of our studio with painted boards, ready to be assembled'
    },
    {
      src: '03.jpg',
      caption: 'A part of our studio with painted boards, ready to be assembled'
    }
  ],
  tags: ['E-commerce', 'Shopify', 'Branding', 'Handcraft'],
  technos: ['Shopify', 'jQuery', 'HTML5', 'CSS3'],
  links: [
    {
      icon: 'tabler:device-analytics',
      title: 'Website Backup',
      url: '/projects/kruze-skateboards/website-backup/index.html'
    }
  ]
}
export default KruzeSkateboards
