const IssegProjectWeek = {
  url: 'project-week',
  createdIn: 2016,
  title: 'Isseg Project Week',
  tagLine:
    'Hackathon organized by the Epitech, Isseg and E-artsup schools in partnership with Bouygues Construction',
  description: [
    'This project took place in two parts, a first part of one week which took place in several big cities of France, gathering three schools of the Ionis group: Epitech (Technical), Isseg (Marketing & Communication), and E-artsup (Design). And a second part for the final between the winners of each city in Paris a week later. All of this must be done in interdisciplinary teams.',
    'The topic given by Bouygues construction was to design a solution to create the eco-friendly neighborhoods of the future.',
    'We had done some research and saw that there was a lot of electrical energy wasted on standby devices. We also had in mind that home automation was an increasingly popular topic in our society.',
    'Our solution consisted in a controller to be integrated in each housing of future eco districts. This controller could manage all electrical installation of a housing, but also bring him all the latest home automation. The box could control mainly the electrical outlets of the housing to deactivate those where there was a device on standby consuming unnecessary electricity.',
    'At first you just had to select on a web interface the important outlets of the housing like the one of the fridge or a freezer. Once this setting was done, you just had to click on a switch (like the ones to turn off the lights) placed next to the front door each time you left your home so that all the useless sockets of the home on which there were appliances on standby were cut. We had calculated that on the scale of a district that represented on one year of very big savings of electricity.',
    'This was not the only advantage of the controller, which included home automation allowing to control the lights of the house, the temperature, le opening of the shutters, as well as offering internet by Wifi as well as by PLC (through the sockets), and could potentially be LIFI compatible (internet by light).',
    'We won in Toulouse among a dozen of teams of 6 to 8 people, then we participated in the final between all schools in Paris but did not win there.'
  ],
  imgs: [
    {
      src: 'FB_IMG_1458572508847.jpg',
      caption: 'Photo of our win in Toulouse'
    },
    {
      src: '02.jpg',
      caption: 'First prototype I made'
    },
    {
      src: '03.jpg',
      caption: 'Back side of my first prototype'
    },
    // {
    //   src: '05.mp4'
    // }
    {
      src: '06.jpg',
      caption: 'Disassembling of the prototype to extract the parts'
    },
    {
      src: '07.jpg',
      caption: 'Starting to create the second improved prototype'
    },
    {
      src: '08.jpg',
      caption: 'Positioning of the elements'
    },
    {
      src: '09.jpg',
      caption: 'Drilling holes for electrical outlets'
    },
    {
      src: '10.jpg',
      caption:
        'Drilling the hole for the switch, tracing the location of the enclosure'
    },
    {
      src: '11.jpg',
      caption: "Placement of the enclosure's frame"
    },
    {
      src: '12.jpg',
      caption: 'Fixing the brackets'
    },
    {
      src: '13.jpg',
      caption: 'Assembling the enclosure'
    },
    {
      src: '14.jpg',
      caption: 'Painting the frame'
    },
    {
      src: '15.jpg',
      caption: 'Painting the enclosure'
    },
    {
      src: '16.jpg',
      caption: 'Mounting the enclosure'
    },
    {
      src: '17.jpg',
      caption: 'Installation of the outlets and the switch'
    },
    {
      src: '18.jpg',
      caption: 'Fixing the relays'
    },
    {
      src: '19.jpg',
      caption: 'Positioning of the Intel Edison board'
    },
    {
      src: '20.jpg',
      caption: "Assembly the enclosure's door"
    },
    {
      src: '21.jpg',
      caption: 'Wiring of relays to outlets'
    },
    {
      src: '22.jpg',
      caption: 'Back side of the prototype with the wiring'
    },
    {
      src: '23.jpg',
      caption: 'Remounting the Intel Edison board'
    },
    {
      src: '24.jpg',
      caption: 'Connecting relays to the board'
    },
    {
      src: '25.jpg',
      caption: 'Connecting relays to the board'
    },
    {
      src: '26.jpg',
      caption: 'Side view of the prototype'
    },
    {
      src: '27.jpeg',
      caption: 'Prototype control interface'
    },
    {
      src: 'FB_IMG_1458586323718.jpg',
      caption: 'In the theater of the TF1 tower before going on stage'
    },
    {
      src: 'received_10209138233641726.jpeg',
      caption: 'On stage at the TF1 tower'
    },
    {
      src: 'Mail Philipe Coste - Project Week 2016.png',
      caption: 'Congratulations email from the director of Epitech Toulouse'
    }
  ],
  tags: [
    'Hackathon',
    'Winner',
    'Boygues Construction',
    'IoT',
    'Prototyping',
    'Raspberry Pi',
    'Electronics',
    'Sensors'
  ],
  technos: ['Javascript', 'Node.js', 'Express', 'MongoDB', 'HTML5', 'CSS3']
  // links: [
  //   {
  //     icon: 'vscode-icons:file-type-pdf',
  //     title: 'Hackathon Subject',
  //     url: '/projects/project-week/Project-Week-Subject.pdf'
  //   }
  // ]
  // https://www.epitech.eu/fr/actualites-evenements/project-week-2016-finale-bouygues-construction-tf1-iseg-epitech-e-artsup/
  // Bouygues Construction Flexom
}

export default IssegProjectWeek
