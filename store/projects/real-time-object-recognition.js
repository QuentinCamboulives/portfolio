const RealTimeObjectRecognition = {
  url: 'real-time-object-recognition',
  createdIn: 2019,
  title: 'Real Time Object Recognition',
  tagLine:
    'Simple Client/Server application using live image recognition to search for matching images in a database',
  description: [
    'I realized this project alone in one week during my 4th year of Master in China as part of the international exchange organized by Epitech. We had courses on artificial intelligence and we were asked to build a small project using an AI engine of our choice and a database.',
    'I chose to use the TensorFlow Machine Learning engine because I wanted to discover this technology for some time. I used a pre-trained model to recognize 60 different everyday objects. I then developed a script to scrape images from ImageNet to fill my database, and I made it download 400 images of 24 types of common objects recognized by my model such as glasses, bikes, remote controls, smartphones, etc...',
    'Then I developed a client composed of a web page in Angular, running the image recognition engine that I connected to the video stream of the PC webcam. And a server with an API allowing to make requests on the images coming from my database.',
    'When objects were shown to the webcam, the TensorFlow model recognized the object live and the script retrieved its type, then performed a racket of 100 random images from my database and displayed them on the web page.',
    'The project was a great success for my teacher. If you want to have more information, I advise you to look at the PDF report that I gave with the project, it is quite detailed. I have also attached two PDF reports that I had to do during the year for this subject.'
  ],
  imgs: [
    {
      src: '01.jpg',
      caption: 'Live recognition of a remote control by the TensorFlow model'
    },
    {
      src: '02.jpg',
      caption: 'Display of corresponding images in database'
    },
    {
      src: '03.jpg',
      caption: "The project's architecture"
    }
  ],
  tags: [
    'TensorFlow',
    'AI',
    'Computer Vision',
    'Image Recognition',
    'Database',
    'Scrapping'
  ],
  technos: ['TensorFlow', 'Node.js', 'Angular', 'MongoDB', 'HTML5', 'CSS3'],
  links: [
    {
      icon: 'vscode-icons:file-type-pdf',
      title: 'Report of this project',
      url:
        '/projects/real-time-object-recognition/New Generation Database System Report - Quentin Camboulives.pdf'
    },
    {
      icon: 'vscode-icons:file-type-pdf',
      title: 'Computer Vision Report',
      url:
        '/projects/real-time-object-recognition/Digital Image Processing and Computer Vision - Quentin Camboulives.pdf'
    },
    {
      icon: 'vscode-icons:file-type-pdf',
      title: 'Artificial Intelligence Report',
      url:
        '/projects/real-time-object-recognition/Advanced Artificial Intelligence - Quentin Camboulives.pdf'
    }
  ]
}

export default RealTimeObjectRecognition
