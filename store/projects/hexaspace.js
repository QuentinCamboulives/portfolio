const HexaSpace = {
  url: 'hexaspace',
  createdIn: 2022,
  title: 'HexaSpace Metaverse',
  tagLine: 'An open & creative Metaverse for all',
  description: [
    'HexaSpace is a decentralized virtual world that allows users to create, monetize and enjoy immersive experiences together. Our mission is to connect worldwide communities through immersive experiences by facilitating access to the metaverse and encouraging creativity. Live immersive experiences with friends, fans and communities worldwide who share the same passions as you! Create your own identity, collect digital assets, access exclusive content and private events.'
  ],
  imgs: [
    {
      src: 'oww.png',
      caption: 'Inside the Metaverse, in the OMEGA WOMEN WORLD'
    },
    {
      src: 'art-gallery.png',
      caption: 'Inside OMEGA WOMEN WORLD, next to the Art Gallery'
    },
    {
      src: 'shop.png',
      caption: 'Inside OMEGA WOMEN WORLD, in front of a virtual store'
    },
    {
      src: 'oww-high.png',
      caption: 'OMEGA WOMEN WORLD seen from the highest viewpoint'
    },
    {
      src: 'home.png',
      caption: 'HexaSpace home page'
    }
  ],
  tags: [
    'Metaverse',
    'NFT',
    'Web3',
    'Smart Contract',
    '$Token',
    'Blender',
    'Communities',
    'Creativity',
    'Freedom'
  ],
  technos: [
    'Threejs',
    'Solidity',
    'Nuxt.js',
    'Vue.js',
    'Socket.io',
    'Javascript'
  ],
  links: [
    {
      icon: 'tabler:hexagons',
      title: 'HexaSpace',
      url: 'https://hexaspace.io'
    },
    {
      icon: 'ion:game-controller',
      title: 'Metaverse Demo',
      url: 'https://hexaspace.io/space/omega-women-world'
    },
    {
      icon: 'simple-icons:opensea',
      title: 'OpenSea Lands Collection',
      url: 'https://opensea.io/fr/collection/hexaspace-island-of-origins'
    }
  ]
}
export default HexaSpace
