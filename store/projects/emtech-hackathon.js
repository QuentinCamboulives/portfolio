const EmtechHackathon = {
  url: 'emtech-hackathon',
  createdIn: 2015,
  title: 'MIT EmTech Hackathon',
  tagLine:
    'Hackathon organized by the MIT Technology Review in partnership with Toulouse city, Intel and Orange on the thematics of the Smart City and Smart Citizenship',
  description: [
    'This project was developed by a team of three people during a three-day Hackathon bringing together several engineering schools of Toulouse.',
    'The EmTech (Emerging Technologies) organized by the MIT Technology Review is one of the most important conferences dedicated to emerging technologies worldwide. The goal of this conference is to promote new knowledge about technologies and analyze their implications for our society and how they are revolutionizing our daily lives.',
    'In parallel to the EmTech conferences, a Hackathon was organized in partnership with the city of Toulouse on the thematics of the Smart City and Smart Citizenship. Intel and Orange as partners provided electronic hardware, Intel provided Intel Edison computer boards with multiple sensors, and Orange provided LoRa chips allowing the communication of IoT objects over miles of distance while being ultra energy efficient.',
    'The goal of the Hackathon was to create a project aimed at making cities more Eco-citizen while using the equipment provided.',
    "We started from the observation that it is extremely important that people with reduced mobility always have reserved parking spaces allowing them to access urban commodities more easily. However, sometimes people who don't need this type of space still park in it, which impacts the people who really need it.",
    'Our idea was to create a small device to be fixed on poles already present in town, placed in front of reserved parking for disabled people. This device embedded a computer card, as well as a sensor of telemetry by ultra sounds allowing to detect the presence of big objects such as a car or a motorcycle, an NFC antenna, and a LoRa antenna to stay connected to the network.',
    'The mechanism worked as follows: if a car parked in front of the box, the ultrasonic telemetry sensor would detect it, and if after 15 minutes the car was still there, the computer would send a request to a server with its GPS position. An application for the municipal agents was used to display these alerts on a map in order to go to the location and notice the infraction and do what was necessary.',
    'In the case where a disabled person parks on a space equipped with this device, this person must have an NFC card (theoretically delivered by the city of Toulouse in our fictive example). So if the box detected a car on the parking space, but that this car contained the NFC card indicating that the person has the right to use this parking space, no alert was raised.',
    'This project was perfectly in line with the theme of the Hackathon and it was very appreciated by the organizers and the jury.',
    'I advise you to read the paper of La Depeche for more details (link in the Infos section).'
  ],
  imgs: [
    {
      src: '01.png',
      caption:
        'The winning team (all three of us on the left) and the second place team on stage to present their projects'
    },
    {
      src: '15.jpg',
      caption: 'The main entrance'
    },
    {
      src: '16.jpg',
      caption: 'The lobby'
    },
    {
      src: '02.jpg',
      caption: 'The Hackathon space'
    },
    {
      src: '03.jpg',
      caption: 'The Hackathon space'
    },
    {
      src: '07.jpg',
      caption: 'Our prototype under construction'
    },
    {
      src: '08.jpg',
      caption: 'Our prototype under construction'
    },
    {
      src: '05.jpg',
      caption: 'Our prototype almost finished'
    },
    {
      src: '06.jpg',
      caption: 'Our operational prototype'
    },
    {
      src: '09.jpg',
      caption:
        'The small application we made to show the location of the infraction'
    },
    {
      src: '13.jpg',
      caption: 'A picture with Mathieu Nebra, the creator of Open Classroom'
    },
    {
      src: '18.jpg',
      caption: 'The main conference room'
    },
    {
      src: '19.jpg',
      caption: 'The main conference room'
    },
    {
      src: '26.jpg',
      caption: 'On stage to present our project'
    },
    {
      src: '27.jpg',
      caption: 'After our presentation'
    },
    {
      src: '32.jpg',
      caption: 'EmTech partners'
    }
  ],
  tags: [
    'Hackathon',
    'Winner',
    'IoT',
    'Intel Edison',
    'Sensors',
    'Prototyping',
    'Intel',
    'Orange'
  ],
  technos: ['Python', 'Javascript', 'HTML5', 'CSS3'],
  links: [
    {
      icon: 'logos:youtube-icon',
      title: 'EmTech Summary Video',
      url: 'https://www.youtube.com/watch?v=5X0GGppa0AE&ab_channel=EmTechEurope'
    },
    {
      icon: 'ant-design:file-text-outlined',
      title: "Epitech's post",
      url: '/projects/emtech-hackathon/Epitech Post.png'
    },
    {
      icon: 'ant-design:file-text-outlined',
      title: 'La Depeche post',
      url:
        'https://www.ladepeche.fr/article/2015/12/18/2294906-eleves-epitech-toulouse-remportent-hackathon-smart-city-toulouse.html'
    },

    {
      icon: 'ant-design:file-text-outlined',
      title: 'EmTech - La Depeche',
      url:
        'https://www.ladepeche.fr/article/2015/12/12/2294748-la-conference-emtech-consacre-toulouse-capitale-europeenne-de-l-innovation.html'
    },
    {
      icon: 'ant-design:file-text-outlined',
      title: 'EmTech - La Tribune',
      url:
        'https://toulouse.latribune.fr/innovation/french-tech/2015-10-07/pourquoi-toulouse-accueillera-la-premiere-conference-emtech-d-europe.html'
    }
    // https://france3-regions.blog.francetvinfo.fr/numerique-midi-pyrenees/2016/05/03/retour-a-toulouse-demtech-la-plus-importante-conference-internationale-sur-les-technologies-du-futur.html
    // https://www.ionis-group.com/actualites/2016/01/epitech-emtech-france-2015/
    // https://lameleeadour.com/emtech-france-15-et-16-decembre-2015-a-toulouse/
  ]
}

export default EmtechHackathon
