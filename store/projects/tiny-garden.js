const TinyGarden = {
  url: 'tiny-garden',
  createdIn: 2018,
  title: 'Tiny Garden',
  tagLine:
    'A simple Raspberry Pi project to automatically grow kitchen herbs at home',
  description: [
    'IoT project realized alone in about two weeks during my studies at Epitech in the scope of the Hub projects. The Hub was a kind of lab with computer hardware and it was a place where creating projects using electronic hardware was valued. It was nice since I already liked building these kinds of projects.',
    'I wanted to be able to grow herbs for cooking, but without having to think about taking care of them. So I decided to create a small system based on a Raspberry Pi Zero W computer board used to drive different sensors and analogical outputs, 3 RGB rings leds to light up the plants and give them energy, a peristaltic pump for drip irrigation, and a glass tank filled with water. I built a wooden base to group all the elements, and developed a website with Node.js and Express.js served by the Raspberry Pi allowing to monitor the system from anywhere as long as you have an internet connection.',
    'I had investigated the specificities of growth of the main aromatic plants such as parsley, chives, coriander, etc. .... And I had recorded them in a database. For each type of plant, I varied the color of the LED rings according to the information I had found, starting with blue to allow the rapid growth of the seeds, then moving to white and red at the end of the growth period, depending on the number of days since the seeds were planted (the aim was to boost the growth of the plants). It worked very well and the project was very appreciated by the director of studies of the school who made a Tweet on the Twitter account of Epitech Toulouse.'
  ],
  imgs: [
    {
      src: 'IMG_0102.jpg',
      caption:
        'The website allowing to monitor the growth of the plants, next to the project'
    },
    {
      src: 'IMG_1587.jpg',
      caption: 'The website under construction'
    },
    {
      src: 'IMG_1586.jpg',
      caption:
        'A page allowing to take control of the color light of the rings leds'
    },
    {
      src: 'TweetADM.png',
      caption: "The school director's tweet"
    },
    {
      src: 'EpitechTwitter.jpg',
      caption: 'The retweet by the Epitech Toulouse Twitter account'
    }
  ],
  tags: [
    'Automation',
    'IoT',
    'Raspberry Pi',
    'UI',
    'UX',
    'Electronics',
    'Sensors',
    'Prototyping'
  ],
  technos: ['Node.js', 'Express', 'Javascript', 'MongoDB', 'HTML5', 'CSS3'],
  links: [
    {
      icon: 'logos:twitter',
      title: 'Tweet of Epitech Director',
      url: 'https://twitter.com/GildasVinson/status/979615023277408258'
    }
  ]
}

export default TinyGarden
